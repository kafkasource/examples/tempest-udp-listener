/*
 * Copyright (C) 2020 Chad Preisler
 *
 * 
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

package com.kafkasource.tempest;

import java.io.IOException;
import java.net.DatagramPacket;
import java.net.DatagramSocket;
import java.net.SocketException;
import java.net.UnknownHostException;
import java.util.Map;
import java.util.TreeMap;
import java.util.logging.Level;
import java.util.logging.Logger;
import org.apache.kafka.clients.producer.KafkaProducer;
import org.apache.kafka.clients.producer.ProducerConfig;
import org.apache.kafka.clients.producer.ProducerRecord;
import org.apache.kafka.common.serialization.StringSerializer;
import org.json.JSONException;
import org.json.JSONObject;

/**
 *
 * @author Chad Preisler
 */
public class TempestUdpListener {
    private static final String TOPIC_NAME = "kafkasource.tempest-raw-data";
    private static final Logger LOGGER = Logger.getLogger(TempestUdpListener.class.getName());

    public static void main(String[] args) throws SocketException, UnknownHostException, IOException {
        var producer = new KafkaProducer(producerProperties());
        var socket = new DatagramSocket(50222);
        socket.setSoTimeout(10000);
        Runtime.getRuntime().addShutdownHook(new Thread(() -> {
            System.out.println("Shutting down application.");
            socket.close();
            producer.flush();
            producer.close();
        }));
        while (true) {            
            var buf = new byte[1024];
            var packet =  new DatagramPacket(buf, buf.length);
            socket.receive(packet);
            var rawPacketData = new String(packet.getData());
            var serialNumber = parse(rawPacketData);
            if (serialNumber != null) {
                producer.send(new ProducerRecord(TOPIC_NAME, serialNumber, rawPacketData));
            } else {
                LOGGER.log(Level.INFO, "Could not pull serial_number from message: {0}", rawPacketData);
            }
        }
    }
    
    private static final String parse (String message) {
        var jsonObject = new JSONObject(message);
        try {
            return jsonObject.getString("hub_sn");
        } catch (JSONException ex) {
            return jsonObject.getString("serial_number");
        }
    }
    
    private static final Map<String, Object> producerProperties() {
        var props = new TreeMap<String, Object>();
        props.put(ProducerConfig.BOOTSTRAP_SERVERS_CONFIG, "localhost:9092");
        props.put(ProducerConfig.KEY_SERIALIZER_CLASS_CONFIG, StringSerializer.class);
        props.put(ProducerConfig.VALUE_SERIALIZER_CLASS_CONFIG, StringSerializer.class);
        return props;
    }    
}
