# Tempest Weather Kafka

Simple program that takes UDP packets broadcast by the Tempest Weather Station 
and puts them into a Kafka topic.

#License 

Copyright (C) 2023 Chad Preisler

 This file is part of kafkasource.visualkafka.

 This program is free software: you can redistribute it and/or modify
 it under the terms of the GNU General Public License as published by
 the Free Software Foundation, either version 3 of the License, or
 (at your option) any later version.

 This program is distributed in the hope that it will be useful,
 but WITHOUT ANY WARRANTY; without even the implied warranty of
 MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 GNU General Public License for more details.

 You should have received a copy of the GNU General Public License
 along with this program.  If not, see <http://www.gnu.org/licenses/>.
